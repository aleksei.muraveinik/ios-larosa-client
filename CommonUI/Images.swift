import UIKit

extension UIImage {
	static let done: UIImage = UIImage(named: "done")!
	static let error: UIImage = UIImage(named: "error")!
}
